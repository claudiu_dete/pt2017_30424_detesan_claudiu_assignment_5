package hw5;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Operations {
	
	List<MonitoredData> monitoredData=new ArrayList<MonitoredData>();
	private final String path="E:\\Facultate\\Activities.txt";
	private final String path1="E:\\Distinct.txt";
	private final String path2="E:\\Log.txt";
	private final String path3="E:\\total.txt";
	private final String path4="E:\\last.txt";
	
	public void readData() 
	{
		
		try{
		FileReader fr=new FileReader(path);
		BufferedReader br=new BufferedReader(fr);
		String str;
		while((str=br.readLine())!=null)
		{
			
			 DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			 String d=str.substring(0, 19);
			
			 Date date1=dateFormat.parse(d);
			 
			 String d1=str.substring(21,39);
			 Date date2=dateFormat.parse(d1);
			 String d2=str.substring(42);
			 MonitoredData mon=new MonitoredData(date1,date2,d2);
			 
			 monitoredData.add(mon);
			 
			
	    }
		br.close();
		fr.close();
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
		
	}
	
	
	public void countDistinctDays()
	{
		Map<String,Long> hours=monitoredData.stream().collect(Collectors.groupingBy(MonitoredData::getTime,Collectors.counting()));
		System.out.println(hours.size());
	}
	
	public void countDistinctActions()
	{
		Map<String,Long> actions= monitoredData.stream().collect(Collectors.groupingBy(MonitoredData::getActivity,Collectors.counting()));
		
		try{
			FileWriter fw=new FileWriter(path1);
			BufferedWriter bw=new BufferedWriter(fw);
			for(Entry<String, Long> key: actions.entrySet())
			{
				String s=key.getKey()+key.getValue();
				bw.write(s);
				bw.newLine();
				
		     }   
			bw.close();
			fw.close();
			
		   }
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void activityCount()
	{
		Map<String,Map<String,Long>> log=monitoredData.stream().collect(Collectors.groupingBy(MonitoredData::getTime,Collectors.groupingBy(MonitoredData::getActivity,Collectors.counting())));
		try
		{
			FileWriter fw=new FileWriter(path2);
			BufferedWriter bw=new BufferedWriter(fw);
			for(Entry<String,Map<String,Long>> entry:log.entrySet())
			{
				bw.write(entry.getKey());
				bw.newLine();
				     for(Entry<String,Long> en: entry.getValue().entrySet())
				     {
				    	 String content=en.getKey()+" "+en.getValue();
				    	 bw.write(content);
				    	 bw.newLine();
				    	 
				    	 
				     }
			}
			
			bw.close();
			fw.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void totalDuration()
	{
		Map<String,Long> duration=monitoredData.stream().collect(Collectors.groupingBy(MonitoredData::getActivity,Collectors.summingLong( n->n.getDateDiff(TimeUnit.HOURS))));
		Map<String,Long> dur= duration.entrySet().stream().filter(m-> m.getValue()>10).collect(Collectors.toMap(p-> p.getKey(), p-> p.getValue()));
		for(Entry<String,Long> entry:dur.entrySet())
		{
			System.out.println(entry.getKey()+" "+entry.getValue());
		}
		try
		{
			FileWriter fw=new FileWriter(path3);
			BufferedWriter bw=new BufferedWriter(fw);
			
			for(Entry<String,Long> entry:dur.entrySet())
			{
				String content=entry.getKey()+" "+entry.getValue();
				bw.write(content);
				bw.newLine();
			}
			bw.close();
			fw.close();
		}
		
		catch(IOException ex)
		{
			ex.printStackTrace();
		}
	}
	
	
	   		
	   		
	   			
		
	
	
	

}
