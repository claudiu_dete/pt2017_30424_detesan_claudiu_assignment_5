package hw5;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class MonitoredData {
	
	private Date startTime;
	private Date endTime;
	private String activityLabel;
	
	public MonitoredData(Date d1,Date d2, String a)
	{
		this.startTime=d1;
		this.endTime=d2;
		this.activityLabel=a;
	}
	
	public String getActivity()
	{
		return this.activityLabel;
	}
    
	public void setActivity(String s)
	{
		this.activityLabel=s;
	}
	
	public Date getStartTime()
	{
		return this.startTime;
	}
	
	public Date getEndTime()
	{
		return this.endTime;
	}
	
	public void setStartTime(Date d)
	{
		this.startTime=d;
	}
	
	public void setEndTime(Date d)
	{
		this.endTime=d;
	}
	
	public boolean equals(Object o)
	{
		return true;
	}
	
	public String getTime()
	{
		return Integer.toString(startTime.getMonth())+"."+Integer.toString(startTime.getDate());
	}
	
	public int getActivityTime()
	{
		if(startTime.getDate()==endTime.getDate())
		{
			return endTime.getHours()-startTime.getHours();
		}
		else
		{
			return endTime.getHours()+24-startTime.getHours();
		}
	}
	
	public long getDateDiff(TimeUnit timeunit)
	{
		long diff=endTime.getTime()-startTime.getTime();
		return timeunit.convert(diff,TimeUnit.MILLISECONDS);
	}

}
